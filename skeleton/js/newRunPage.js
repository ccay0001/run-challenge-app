// Code for the Measure Run page.
// set up map ?? inside Run class or outside Run class ??
  mapboxgl.accessToken = 'pk.eyJ1Ijoic29waGllNjk2OSIsImEiOiJjanptNTJiM3EwNXo4M2dtdHBqb2lzZ3UzIn0.0ZM9nmG-mLjVLCadn1w2ZA';
        let clayton = [145.1343136 , -37.9110467];
    
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: clayton;
        });

  // This should be your own API key
        mapboxgl.accessToken = 'pk.eyJ1Ijoic29waGllNjk2OSIsImEiOiJjanptNTJiM3EwNXo4M2dtdHBqb2lzZ3UzIn0.0ZM9nmG-mLjVLCadn1w2ZA';
    
        let caulfield = [145.0420733, -37.8770097];
    
        let map = new mapboxgl.Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v10',
            zoom: 16,
            center: caulfield
        });
        
                        showCampusCenter();
                        showLecturePosition();

      var x = document.body.getElementById("displayNewRun");
function getLocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
  } else {
    x.innerHTML = "Geolocation is not supported by this browser.";
  }
}

function showPosition(position) {
  x.innerHTML = "Latitude: " + position.coords.latitude +
  "<br>Longitude: " + position.coords.longitude;
}        

                      
                        
            
//var id = navigator.geolocation.watchCurrentPosition(success, error, options);
//  showLecturePosition();
//function success(currentPosition) {
//  
//
//            let marker = new mapboxgl.Marker({
//                color: "blue"
//            });
//            marker.setLngLat(currentPosition);
//            marker.addTo(map);
//            
//            // Display a label above the lecture position pin.
//            let popup = new mapboxgl.Popup({
//                offset: 40
//            });
//            popup.setText("You are here!");
//            marker.setPopup(popup);
//    navigator.geolocation.clearWatch(id);
//  }
//
//
//function error(err) {
//  alert('ERROR(' + err.code + '): ' + err.message);
//}
//
//options = {
//  enableHighAccuracy: true,
//  timeout: 5000,
//  maximumAge: 200
//};
//
// 
            


//            // Centre the map on the lecture location and increase zoom.
//            map.easeTo({
//                center: currentPosition,
//                zoom: 18
//            });
            
        
        // Code added here will run whenthe page loads.
        function showLecturePosition()
        {
            //// Show a pin at the lecture location.
            let lecturePosition = [145.1339903, -37.9109989];
            let marker = new mapboxgl.Marker({
                color: "green"
            });
            marker.setLngLat(lecturePosition);
            marker.addTo(map);
            
            // Display a label above the lecture position pin.
            let popup = new mapboxgl.Popup({
                offset: 40
            });
            popup.setText("Lecture Position!");
            marker.setPopup(popup);

            // Centre the map on the lecture location and increase zoom.
//            map.easeTo({
//                center: lecturePosition,
//                zoom: 18
//            });
        }

        
        function showMonashSport()
        {
            let MonashSport = [145.1362585, -37.9128781];
            let marker = new mapboxgl.Marker({
                color: "yellow"
            });
            marker.setLngLat(MonashSport);
            marker.addTo(map);
            
            // Display a label above the lecture position pin.
            let popup = new mapboxgl.Popup({
                offset: 50
            });
            popup.setText("Monash Sport!");
            marker.setPopup(popup);
        }
        
        
        
        function showCampusCenter()
        {
            let two = 2;
            let campusCenter = [145.1329004, -37.9118667];
            let marker = new mapboxgl.Marker({
                color: "red"
            });
            marker.setLngLat(campusCenter);
            marker.addTo(map);
            
            // Display a label above the lecture position pin.
            let popup = new mapboxgl.Popup({
                offset: 50
            });
            popup.setText("Campus Center!" + two);
            marker.setPopup(popup); 
        }
        
        

        // This function checks whether there is a map layer with id matching 
        // idToRemove.  If there is, it is removed.
        function removeLayerWithId(idToRemove)
        {
            let hasPoly = map.getLayer(idToRemove)
            if (hasPoly !== undefined)
            {
                map.removeLayer(idToRemove)
                map.removeSource(idToRemove)
            }
        }

class Run {


    constructor()
    {
        // function of navigator.geolocation 
        //a second callback function is executed if an error occurs. 
        //A third, optional, parameter is an options object ,set the maximum age of the position returned, the time to wait for a request, and high accuracy for the position.
        // this function below is not as suitable as watch position
        navigator.geolocation.getCurrentPosition(function(position) {
        this._currentLocation = [position.coords.latitude, position.coords.longitude];}, geo_error)
        // startLocation
        this._startLocation = this._currentLocation;

        // set up time at the start of the new Run
        var today = new Date();

        this._startDate = today.getDate() +"-"+(today.getMonth()+1)+"-"+today.getFullYear();

        // set up starting time for the run 
        this._startTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        this._startDateTime = this._startDate+ " "+ this._startTime;

    };
// necessary information

    // public methods

    function setEndDateTime()
    {
        var today = new Date();
        // getFullYear() – Provides current year like 2018.
        // getMonth() – Provides current month values 0-11. Where 0 for Jan and 11 for Dec. So added +1 to get result.
        // getDate() – Provides day of the month values 1-31.
        this._endDate = today.getDate() +"-"+(today.getMonth()+1)+ "-"+today.getFullYear();

        // set up finishing time for the run 
        this._endTime = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

        this._endDateTime = this._endDate+ " "+ this._endTime;
    };

    function getCurrentLocation()
    {
        return this._currentLocation;
    }

    function getStartDate()
    {
        return this._startDate;
    };

    function getStartTime()
    {
        return this._startTime;
    };

    function getStartDateTime()
    {
        return this._startDateTime;
    };

    function getEndTime()
    {
        return this._endTime;
    };

    function getEndDate()
    {
        return this._endDate;
    };

    function getEndDateTime()
    {
        return this._endDate + "-" + this._startTime;
    };

    function getCurrentLocaion()
    {
    return this._currentLocation;
    }
}
// display any position that it takes as its argument
    function displayPosition(position)
    {
        let currentPosition = [position.coords.latitude, position.coords.longitude];
                
        let marker = new mapboxgl.Marker
        ({
            color: "blue"
        });
        marker.setLngLat(currentPosition);
        marker.addTo(map);
            
        // Display a label above the current position pin.
        let popup = new mapboxgl.Popup
        ({
            offset: 40
        }); 
        popup.setText("You are here!");
        marker.setPopup(popup);

//            // Centre the map on the current location and increase zoom.
//            map.easeTo({
//                center: currentPosition,
//                zoom: 18
//            });
    }

// those functions below are used for Watch Position function 
    function geo_success(position) 
    {
        displayPosition(position);
        // then we have to save this coordinate in an array which stores all location in one Run instance, in order
        // to keep track of user's path

    }

    function geo_error() 
    {
        alert("ERROR: Couldn't identify your location");
    }

// time waited for request is 10 seconds
// maximumAge ??
    var geo_options = {
          enableHighAccuracy: true, 
          maximumAge        : 30000, 
          timeout           : 10000
    };

    var keepTrack = navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
// use clear interval when the run has finished
// navigator.geolocation.clearWatch(keepTrack);

// displays current position and doesn't need any arguments
    function displayCurrentPosition()
    {
        let currentPosition = [];
        navigator.geolocation.getCurrentPosition(function(position) {
        currentPosition = [position.coords.latitude, position.coords.longitude];}, geo_error);

        let marker = new mapboxgl.Marker
        ({
            color: "red"
        });
        marker.setLngLat(currentPosition);
        marker.addTo(map);
            
        // Display a label above the current position pin.
        let popup = new mapboxgl.Popup
        ({
            offset: 40
        });
            
        popup.setText("You are here!");
        marker.setPopup(popup);

//            // Centre the map on the current location and increase zoom.
//            map.easeTo({
//                center: currentPosition,
//                zoom: 18
//            });
    }



//Sophie
// There should be a button on the app’s New Run page which can be tapped to generate a new destination 
// if (user accuracy drops down to 20m for the first time)
var ranDestButton = document.body.getElementById("randDestButton");
// id randDestButton is Generate New Destination button's id in newRun.html
ranDestButton.onclick = randomDestination(Run.getCurrentLocation());

function randomDestination(position)
{
    let currentLong = position.coords.longitude;
    let currentLat = position.coords.latitude;
    //get the random distance between 60 to 150
    let ranDistance = Math.floor(Math.random()*91) + 60;  
    //get the random angle between 0 to 360 
    let ranAngle = Math.floor(Math.random()*361);  
     //The constant is 1 meter in latitude and longitude
    const  constant = 0.0000094057;     

    // get new longitude
    let newLong = currentLong + ranDistance * Math.sin(ranAngle * Math.PI / 180) * constant ; 
    // get new latitude 
    let newLat = currentLat + ranDistance * Math.cos(ranAngle * Math.PI / 180) * constant ;  

    let newPosition = [newLat,newLong];
    
    // display the new location using green marker 
    // and estimated distance from current location to destination
    let marker = new mapboxgl.Marker
    ({
        color: "green"
    });
    marker.setLngLat(newPosition);
    marker.addTo(map);

            // Display a label above the current position pin.
    let popup = new mapboxgl.Popup
    ({
        offset: 45
    });
    popup.setText("New Destination is here! \n Estimated distance : "+ ranDistance);
    marker.setPopup(popup);

        // Centre the map on the NEW RANDOM location and increase zoom.
        map.easeTo
        ({
            center: position
            zoom: 18
        });
    
}
